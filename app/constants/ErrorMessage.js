module.exports = {
    STATUS_400: {
        TITLE_REQUIRE: 'Invalid of missing parameter',
        REQUIRED: 'is required.',
        INCORRECTED: 'is incorrected.',
    },
    STATUS_401: {
        REVOKE: 'Token has been revoke.',
        INVALID_SIGNATURE: 'Invalid signature',
    },
    STATUS_404: 'Not Found.',
    STATUS_405: 'Method Not Allowed.',
    STATUS_500: 'Internal Server Error.',
    STATUS_503: 'Service is Under Maintenance.',
};
