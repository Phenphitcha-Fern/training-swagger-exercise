require('express-async-errors');
require('dotenv').config();
const config = require('config');
const express = require('express');
const app = express();
const routes = require('./routes/Router');

// Docs

// Main
app.use('/api', routes);

const port = config.leaveService.port;

app.listen(port, () => {
    console.log(
        `Listening on port ${port}..., at ${
            config.name
        } on ${new Date().toISOString()}`
    );
});
