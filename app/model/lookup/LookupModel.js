const config = require('config');
const sql = require('mssql');
const _ = require('lodash');
const { SP_GET_LUT } = require('../../constants/StoreProcedures');
const MSSQLConnector = new (require('../../utils/MSSQLConnector'))(
    config.leaveService.sqlDB
);

const getLut = async (tableName, language = 'th', leaveTypeId) => {
    try {
        let storeParams = [
            {
                name: 'PI_LANGUAGE',
                type: sql.NVarChar,
                value: language
            },
            {
                name: 'PI_TABLE',
                type: sql.NVarChar,
                value: tableName
            },
            {
                name: 'PI_LEAVE_TYPE_ID',
                type: sql.NVarChar,
                value: leaveTypeId
            }
        ];

        let result = await MSSQLConnector.executeStore(SP_GET_LUT, storeParams);

        let data = formatResult(result.recordsets[0]);

        return data;
    } catch (error) {
        throw error;
    }
};

const formatResult = dataSet => {
    let result = dataSet.map(record => {
        let item = _.mapValues(record, function(attribute) {
            return attribute === null
                ? ''
                : attribute === 'True'
                ? true
                : attribute === 'False'
                ? false
                : attribute;
        });

        return item;
    });

    return result;
};

module.exports = getLut;
