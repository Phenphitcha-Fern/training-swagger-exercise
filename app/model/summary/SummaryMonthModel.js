const config = require('config');
const sql = require('mssql');
const {
    SP_GET_LEAVE_SUMMARY_OF_THE_MONTH
} = require('../../constants/StoreProcedures');
const MSSQLConnector = new (require('../../utils/MSSQLConnector'))(
    config.leaveService.sqlDB
);

const getSummary = async (uuid, year, month, language = 'th') => {
    try {
        let storeParams = [
            {
                name: 'PI_UUID',
                type: sql.NVarChar,
                value: uuid
            },
            {
                name: 'PI_YEAR',
                type: sql.NVarChar,
                value: year
            },
            {
                name: 'PI_MONTH',
                type: sql.NVarChar,
                value: month
            },
            {
                name: 'PI_LANGUAGE',
                type: sql.NVarChar,
                value: language
            }
        ];

        let result = await MSSQLConnector.executeStore(
            SP_GET_LEAVE_SUMMARY_OF_THE_MONTH,
            storeParams
        );
        let data = result.recordsets[0];

        return data;
    } catch (error) {
        throw error;
    }
};

module.exports = getSummary;
