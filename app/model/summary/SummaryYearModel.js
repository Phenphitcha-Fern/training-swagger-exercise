const config = require('config');
const sql = require('mssql');
const _ = require('lodash');
const { SP_GET_SUMMARY_YEAR } = require('../../constants/StoreProcedures');
const MSSQLConnector = new (require('../../utils/MSSQLConnector'))(
    config.leaveService.sqlDB
);

const getSummary = async ({
    uuid,
    year,
    type = 'evaluate',
    language = 'th'
}) => {
    let storeParams = [
        {
            name: 'PI_UUID',
            type: sql.NVarChar,
            value: uuid
        },
        {
            name: 'PI_YEAR',
            type: sql.NVarChar,
            value: year
        },
        {
            name: 'PI_TYPE',
            type: sql.NVarChar,
            value: type
        },
        {
            name: 'PI_LANGUAGE',
            type: sql.NVarChar,
            value: language
        }
    ];

    let spResult = await MSSQLConnector.executeStore(
        SP_GET_SUMMARY_YEAR,
        storeParams
    );

    const leaveTypeList = _.get(spResult, 'recordsets[0]', []);
    const countList = _.get(spResult, 'recordsets[1]', []);

    const result = formatResult(leaveTypeList, countList);
    return result;
};

const formatResult = (leaveTypeList, countList) => {
    const result = leaveTypeList.map(leaveType => {
        const numberOfLeave = countList.filter(item => {
            return item.leaveTypeId === leaveType.leaveTypeId;
        });
        const count = _.get(numberOfLeave, '[0].count', 0);
        const item = {
            leaveTypeId: leaveType.leaveTypeId,
            leaveTypeName: leaveType.leaveTypeName,
            count: count,
            unit: leaveType.unit
        };
        return item;
    });
    return result;
};

module.exports = getSummary;
