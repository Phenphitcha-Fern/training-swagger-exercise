const express = require('express');
const router = express.Router();

const LookupController = require('../controller/LookupController');
const SummaryController = require('../controller/SummaryController');

router.use('/lookup', LookupController);

router.use('/summary', SummaryController);

module.exports = router;
