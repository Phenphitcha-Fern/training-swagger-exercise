const Joi = require('@hapi/joi');

module.exports.getSummarySchema = {
    query: {
        uuid: Joi.string()
            .max(36)
            .required(),
        year: Joi.string().regex(/^\d{4}$/),
        month: Joi.string().regex(/^(0?[1-9]|1[012])$/)
    },
    params: {},
    body: {}
};

module.exports.getSummaryMonthSchema = {
    query: {
        uuid: Joi.string()
            .max(36)
            .required(),
        year: Joi.string()
            .regex(/^\d{4}$/)
            .required(),
        month: Joi.string()
            .regex(/^(0?[1-9]|1[012])$/)
            .required()
    },
    params: {},
    body: {}
};

module.exports.getSummaryYearSchema = {
    query: {
        uuid: Joi.string()
            .max(36)
            .required(),
        year: Joi.string()
            .regex(/^\d{4}$/)
            .optional(),
        // month: Joi.string().when('year', {
        //     is: Joi.string().exist(),
        //     then: Joi.string()
        //         .regex(/^(0?[1-9]|1[012])$/)
        //         .required()
        // }),
        type: Joi.string().optional()
    },
    params: {},
    body: {}
};
