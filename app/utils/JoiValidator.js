const joi = require('@hapi/joi');
const _ = require('lodash');

/**
 *
 * @param {{query, params, body, payload, content}} input
 * @param {{query, params, body, payload, content}} schema
 */
const schemaValidate = (input, schema) => {
    let joiError = [];
    if (schema.query) {
        joiError.push(
            joi.validate(input.query, schema.query, {
                abortEarly: false
            }).error
        );
    }
    if (schema.params) {
        joiError.push(
            joi.validate(input.params, schema.params, {
                abortEarly: false
            }).error
        );
    }
    if (schema.body) {
        joiError.push(
            joi.validate(input.body, schema.body, {
                abortEarly: false
            }).error
        );
    }

    // for generate token schema for jwt payload
    if (schema.payload) {
        joiError.push(
            joi.validate(input.payload, schema.payload, {
                abortEarly: false
            }).error
        );
    }

    // for subscribe message queue
    if (schema.content) {
        joiError.push(
            joi.validate(input.content, schema.content, {
                abortEarly: false
            }).error
        );
    }

    let errors;
    joiError.forEach(error => {
        if (error) {
            if (!errors) {
                errors = { details: [], sources: [] };
            }
            errors.details.push(
                ...error.details.map(error => _.pick(error, ['message']))
            );
            errors.sources.push(...error.details.map(error => error.path[0]));
        }
    });

    if (errors) {
        return errors;
    } else return;
};

module.exports = schemaValidate;
