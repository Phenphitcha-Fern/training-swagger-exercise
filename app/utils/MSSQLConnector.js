const sql = require('mssql');

class SQLConnector {
    constructor(config) {
        this.pools = new sql.ConnectionPool(config, error => {
            if (error !== null) {
                error.library = 'MSSQL';
                console.log('connect to log db error: ', error);
            } else {
                console.log('Connected to MSSQL:', config.server);
            }
        });
    }

    async executeStore(store, storeParams) {
        await this.pools;
        let pool = new sql.Request(this.pools);
        if (storeParams) {
            storeParams.forEach(param => {
                pool.input(param.name, param.type || sql.NVarChar, param.value);
            });
        }
        let result = await pool.execute(store);
        return result;
    }

    health() {
        return { connected: this.pools.connected };
    }
}

let sqlManager = null;
class MSSQLConnector {
    constructor(config) {
        this.reconnect(config);
    }
    async executeStore(store, storeParams) {
        let result;
        try {
            result = await sqlManager.executeStore(store, storeParams);
        } catch (error) {
            error.library = 'MSSQL';
            if (error.code === 'ECONNCLOSED') {
                sqlManager = new SQLConnector(this.config);
                console.log('Reconnecting to MSSQL:', this.config.server);
            }
            throw error;
        }
        if (
            result.recordsets &&
            result.recordsets[0] &&
            result.recordsets[0][0] &&
            result.recordsets[0][0].error
        ) {
            let storeError = new Error(
                `${store}: ${result.recordsets[0][0].error}`
            );
            throw storeError;
        }
        return result;
    }

    async reconnect(config) {
        this.config = config;
        if (!sqlManager) {
            sqlManager = new SQLConnector(config);
            console.log('Connecting to MSSQL:', this.config.server);
        }
    }

    health() {
        return sqlManager.health();
    }
}
module.exports = MSSQLConnector;
